// ====================================================================
// This file is released under the 3-clause BSD license. See COPYING-BSD.
// ====================================================================
function cleanmacros()

    function cleanlib(libpath)
        
        binfiles = ls(libpath+"/*.bin");
        for i = 1:size(binfiles,"*")
            mdelete(binfiles(i));
        end
    
        mdelete(libpath+"/names");
        mdelete(libpath+"/lib");
    endfunction

    libpath = get_absolute_file_path("cleanmacros.sce");
    cleanlib(libpath);
    cleanlib(libpath + filesep() + "sci_codegen");
    cleanlib(libpath + filesep() + "sci_library");

endfunction

cleanmacros();
clear cleanmacros; // remove cleanmacros on stack

// ====================================================================
