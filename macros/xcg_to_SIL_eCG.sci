// Copyright 2016 - Scilab Enterprises - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function xcg_SIL_eCG(blk, scs_m)
    // Generate a Software-In-the-Loop block using "emmtrix Code Generator"
    // 
    // blk: a scicos_block to change
    // scs_m: the diagram to update (containing blk)
    
    // temp working directory for all tools
    tmp = tempname("sil");
    deletefile(tmp);
    createdir(tmp);
    
    // emit scilab code
    [ok,XX,flgcdgen,szclkINTemp,freof] = xcg_to_scilab_coder(blk,scs_m, tmp);
    
    // emit C code using emmtrix CodeGen
    if ok then
        in = ls(tmp + filesep() + "*.sci");
        entrypoints = basename(in);
        args = struct();
        args.outdir = tmp;
        [rep,ok,err] = emx_codegen(in, args);
    end
    
    // compile and link the C code
    if ok then
        cwd = pwd();
        cd(tmp)
        ilib_for_link(entrypoints, ls("*.c"), "", "c");
        exec("loader.sce");
        cd(cwd)
    end
    
    // update the block
    if ok then
        xcosUpdateBlock(XX);
    end
endfunction
