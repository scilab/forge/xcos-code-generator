// Copyright 2017 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function [period, t0] = xcg_get_period(Info)
    // get the current period and initialization from the attached evtdly block
    cpr = Info(2);
    xcs = Info(6);

    // xcs.pointi should be resolved to the right event on the caller function
    event = xcs.pointi;
    
    synchronous = cpr.sim.ordclk(cpr.sim.ordptr(event):cpr.sim.ordptr(event+1)-1, 1);
    all_clocks = find(cpr.sim.clkptr(2:$) - cpr.sim.clkptr(1:$-1));
    blk_period = intersect(synchronous, all_clocks);
    
    // using SampleCLK the blk_period contains both ifthel and m_frequ blocks
    // ifthel should be filtered out
    blk_period = blk_period($);
    
    select cpr.sim.funs(blk_period)
    case "evtdly4" then
        // classical clock
        rpptr_period = cpr.sim.rpptr(blk_period):cpr.sim.rpptr(blk_period+1) - 1;
        period = cpr.sim.rpar(rpptr_period(1));
        t0 = cpr.sim.rpar(rpptr_period(2));
    case "m_frequ" then
        // SampleCLK clock
       opptr_period = cpr.sim.opptr(blk_period):cpr.sim.opptr(blk_period+1) - 1;
       period = cpr.sim.opar(opptr_period(2));
       t0 = cpr.sim.opar(opptr_period(3));
    else
        error("Unsupported event generator " +cpr.sim.funs(blk_period) + " ; use a CLOCK_c instead");
    end
endfunction

