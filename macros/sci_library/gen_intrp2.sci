// Copyright 2017 - Scilab Enterprises - Clement DAVID
// Copyright 2017-2018 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_intrp2(blk_i, variables, Info, flag, fd)
    select flag
    case 1
        gen_write_comment(blk_i, Info, fd);

        v_in_x = variables(cpr.sim.inplnk(cpr.sim.inpptr(blk_i)));
        v_in_y = variables(cpr.sim.inplnk(cpr.sim.inpptr(blk_i)+1));
        v_out = variables(cpr.sim.outlnk(cpr.sim.outptr(blk_i)));

        rpar = cpr.sim.rpar(cpr.sim.rpptr(blk_i):cpr.sim.rpptr(blk_i+1)-1);
        ipar = cpr.sim.ipar(cpr.sim.ipptr(blk_i):cpr.sim.ipptr(blk_i+1)-1);

        // algorithm taken from intrpl2 with more explicit parameters
        mputl("    intrp2_x = " + sci2exp(rpar(1:ipar(1))) + ";", fd);
        mputl("    intrp2_y = " + sci2exp(rpar((ipar(1)+1):(ipar(1)+ipar(2)))) + ";", fd);
        mputl("    intrp2_z = " + sci2exp(matrix(rpar((ipar(1)+ipar(2)+1):$), ipar(1), ipar(2))) + ";", fd);
        mputl("    ", fd);

        mputl("    //EMX?: emx_size(intrp2_i, 1, 1)", fd);
        mputl("    if " + v_in_x + " > intrp2_x($) then", fd);
        mputl("        intrp2_i = size(intrp2_x, ''r'');", fd);
        mputl("    else", fd);
        mputl("        intrp2_i = find(" +   v_in_x + " <= intrp2_x(2:$), 1) + 1;", fd);
        mputl("    end", fd);
        mputl("    //EMX?: emx_size(intrp2_j, 1, 1)", fd);
        mputl("    if " + v_in_y + " > intrp2_y($) then", fd);
        mputl("        intrp2_j = size(intrp2_y, ''r'');", fd);
        mputl("    else", fd);
        mputl("        intrp2_j = find(" + v_in_y + " <= intrp2_y(2:$), 1) + 1;", fd);
        mputl("    end", fd);
        mputl("    ", fd);

        mputl("    intrp2_vx1 = intrp2_x(intrp2_i - 1);", fd);
        mputl("    intrp2_vx2 = intrp2_x(intrp2_i);", fd);
        mputl("    intrp2_vy1 = intrp2_y(intrp2_j - 1);", fd);
        mputl("    intrp2_vy2 = intrp2_y(intrp2_j);", fd);
        mputl("    intrp2_vz1 = intrp2_z(intrp2_j - 1, intrp2_i - 1);", fd);
        mputl("    intrp2_vz4 = intrp2_z(intrp2_j, intrp2_i - 1);", fd);
        mputl("    intrp2_vz2 = intrp2_z(intrp2_j - 1, intrp2_i);", fd);
        mputl("    intrp2_vz3 = intrp2_z(intrp2_j, intrp2_i);", fd);
        mputl("    " + v_out + " = (1 - (" + v_in_y + " - intrp2_vy1) ./ (intrp2_vy2 - intrp2_vy1)) .* (intrp2_vz1 + (intrp2_vz2 - intrp2_vz1) .* (" + v_in_x + " - intrp2_vx1) ./ (intrp2_vx2 - intrp2_vx1)) + ((" + v_in_y + " - intrp2_vy1) ./ (intrp2_vy2 - intrp2_vy1)) .* (intrp2_vz4 + (intrp2_vz3 - intrp2_vz4) .* (" + v_in_x + " - intrp2_vx1) ./ (intrp2_vx2 - intrp2_vx1));", fd);
    end
endfunction

