// This file is released under the 3-clause BSD license. See COPYING-BSD.


function versions = emx_codegen_versions()
	baseurl = 'http://codegen.service.emmtrix.com/';

	versions = emx_getURLjson(baseurl + 'versions.json');
endfunction
