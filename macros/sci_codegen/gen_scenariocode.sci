// Copyright 2016 - Scilab Enterprises - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_scenariocode(in, out, blocks, variables, Info, layer, fd, tows)
    // Generate a default launcher script
    //
    // Parameters
    //  in: vector describing the inputs
    //  out: vector describing the outputs
    //  blocks: vector describing all the blocks (including inputs and outputs)
    //  variables: vector describing the links as variable names
    //  Info: compiled information
    //  layer: selected layer to generate the code for
    //  fd: the file descriptor
    //  tows: the TOWS_c blocks variables names
    
    cpr = Info(2);
    xcs = Info(6);
    
    //////////////////////////
    // Retrieve information //
    //////////////////////////
    
    // retrieve the state size information
    z = cpr.sim.zptr(blocks+1) - cpr.sim.zptr(blocks);
    n_z = sum(z > 0);

    // Scilab variable definition
    if out <> [] then
        outputs = strcat(variables(cpr.sim.outlnk(cpr.sim.outptr(out))), ', ')
        outputs_with_t = ", ";
    else
        outputs = "";
        outputs_with_t = "";
    end
    if in <> [] then
        inputs = strcat(variables(cpr.sim.outlnk(cpr.sim.outptr(in))), ', ');
        if outputs <> "" then
            pre_inputs = ", " + inputs;
        else
            pre_inputs = inputs;
        end
        
        inputs_with_t = ", " + inputs;
    else
        inputs = ""
        pre_inputs = ""
        inputs_with_t = "";
    end
    if n_z > 0 then
        dstate = ", " + strcat("z" + string(blocks(find(z))), ', ');
    else
        dstate = "";
    end
    
    //////////////////////////////////////////////
    // The default scenario is to run on a loop //
    //////////////////////////////////////////////
    
    [period, t0] = xcg_get_period(Info);
    
    // init
    gen_initcode(in, out, blocks, variables, Info, layer, fd);
    
    // run one timeframe
    mputl("", fd);
    
    // retrieve inputs
    if in <> [] then
        mputl("// generating inputs", fd);
        gen_inputscode(in, out, blocks, variables, Info, layer, fd);
    end
    
    // execute
    mputl("", fd);
    if isempty(outputs) & isempty(dstate) then
        tows = part(tows, 3:$); // Avoid initial comma
    end
    mputl("[" + outputs + dstate + tows + "] = " + layer_name + "(t" + inputs_with_t + dstate + ");", fd);
    mputl("", fd);
    
    // pass outputs
    if out <> [] then
        mputl("// print out values", fd);
        gen_outputscode(in, out, blocks, variables, Info, layer, fd);
    end
    
    mputl("", fd);
endfunction
