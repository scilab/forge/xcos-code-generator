[libnames, libpath] = libraryinfo("xcos_code_generatorlib");
path = libpath + "/../tests/unit_tests";

loadXcosLibs();
importXcosDiagram(path + "/scifunc_computation.zcos");

// generate some code for each superblock on TMPDIR
name = "scifunc_computation";
deletefile(TMPDIR + filesep() + name + "_inner1.sci");
deletefile(TMPDIR + filesep() + name + "_inner2.sci");
deletefile(TMPDIR + filesep() + name + "_inner3.sci");

ok = xcg_codegenerator(scs_m, TMPDIR, name);

exec(TMPDIR + filesep() + name + ".sci", 1);
exec(TMPDIR + filesep() +"scifunc_inner1.sci", 1);
exec(TMPDIR + filesep() +"scifunc_inner2.sci", 1);
exec(TMPDIR + filesep() +"scifunc_inner3.sci", 1);

emx_codegen(TMPDIR + filesep() + name + "_scenario.sce");
