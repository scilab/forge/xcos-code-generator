[libnames, libpath] = libraryinfo("xcos_code_generatorlib");
path = libpath + "/../tests/unit_tests";

loadXcosLibs();
importXcosDiagram(path + "/timblk.zcos");

// generate some code for each superblock on TMPDIR
name = "timblk";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

name = "timblk_multiclock";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

name = "timblk_SampleCLK";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

emx_codegen(TMPDIR + filesep() + name + "_scenario.sce");
