//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function subdemolist = demo_gateway()
    demopath = get_absolute_file_path("xcos_code_generator.dem.gateway.sce");

    subdemolist = ["Matrix multiplication script",  "matrixmul_compile.dem.sce" ; ..
                   "Multiple scripts",              "multifile_compile.dem.sce" ; ..
                   "Reduced example",               "SampleCodeGen.dem.sce"];

    subdemolist(:,2) = demopath + subdemolist(:,2);

endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
