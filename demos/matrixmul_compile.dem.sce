//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

chdir(get_absolute_file_path("matrixmul_compile.dem.sce"));

if ~isdir(fullfile(TMPDIR, 'build')) then
    createdir(fullfile(TMPDIR, 'build'))
end

emx_codegen('matrixmul.sce', struct('outdir', fullfile(TMPDIR, 'build')));
scinotes(['matrixmul.sce' fullfile(TMPDIR, 'build', 'matrixmul.c')]);
